---
nom: La Rayonne
logo: https://larayonne.org/wp-content/uploads/2023/05/RAYONNE-rouge.png
photo: https://larayonne.org/wp-content/uploads/2023/12/148811bff79fe6cddb804e9b48b2d5d7.jpg
location: VILLEURBANNE
---

<!-- logo -->
![La Rayone](https://larayonne.org/wp-content/uploads/2023/05/RAYONNE-rouge.png)
<!-- sous-titre -->
La pépinière de La Rayonne, c'est 680 m2 d'espace dans l'Autre Soie pour se former, travailler, créer ou encore s'amuser. Situé en bordure de parc, c'est un lieu qui se veut inspirant pour toutes et tous.

# Pépinière de La Rayonne

<!-- Photo -->
![](https://larayonne.org/wp-content/uploads/2023/12/148811bff79fe6cddb804e9b48b2d5d7.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
**Le CCO vient d’ouvrir La Rayonne, son nouveau tiers-lieu culturel situé au cœur de l'Autre Soie. C’est un lieu vivant où vous pouvez bénéficier d’un accompagnement et de services pour votre projet culturel, social ou environnemental. C’est aussi un lieu d’innovation et de partage, où chacun·e est invité à expérimenter et à contribuer au projet associatif du CCO.**


## Services
**Coworking ponctuel :**
Vous êtes de passage ? Vous souhaitez changer d’air de temps en temps ? Que vous soyez salarié·e, auto-entrepreneur·e, en recherche d’emploi ou encore étudiant, l’open-space nomade vous ouvre ses portes pour un usage à la journée.
*Pas de poste de travail attribué, vous vous installez où il y a de la place, selon votre humeur du jour.*

**Coworking mensuel :**
Vous recherchez un poste de travail pérenne ? Vous souhaitez intégrer une communauté d’entraide pour mener à bien ou développer vos activités ?

Plus d'information : ([cliquez ici](https://larayonne.org/coworking-domiciliation/))
## Localisation
L’Autre Soie - 28 r ue Alfred de Musset 69100 Villeurbanne

https://larayonne.org/coworking-domiciliation/

Pour nous contacter : bonjour@larayonne.org
